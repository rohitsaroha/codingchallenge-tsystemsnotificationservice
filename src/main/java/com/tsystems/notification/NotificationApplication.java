package com.tsystems.notification;

import com.tsystems.notification.config.AppConfig;
import com.tsystems.notification.externalClients.ClientDetailsService;
import com.tsystems.notification.utilities.HeadersInterceptor;
import com.tsystems.notification.utilities.SpringWebClientErrorDecoder;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NotificationApplication {
    @Autowired
    private AppConfig appConfig;
	private Feign.Builder feignClientBuilder() {
		return Feign.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.logger(new Slf4jLogger())
				.logLevel(Logger.Level.BASIC)
				.options(new Request.Options(appConfig.getClientDetails().getFeignConnectTimeout(),appConfig.getClientDetails().getFeignReadTimeout()))
				.errorDecoder(new SpringWebClientErrorDecoder())
				.requestInterceptor(new HeadersInterceptor())
				;
	}
	@Bean
	public ClientDetailsService clientDetailsServiceClient() {
		return feignClientBuilder()
				.requestInterceptor(new BasicAuthRequestInterceptor(appConfig.getClientDetails().getUserName(), appConfig.getClientDetails().getPassword()))
				.target(ClientDetailsService.class, appConfig.getClientDetails().getFeignUrl());
	}
	public static void main(String[] args) {
		SpringApplication.run(NotificationApplication.class, args);
	}

}
