package com.tsystems.notification.services;

import com.tsystems.models.notificationMessage.INotificationMessage;
/*
The imple of this interface will fullfill the message transformation or security responsibilities if required
or need comes can interact with multiple impl of differenct stream implementations
 */
public interface IStreamNotificationService {
    INotificationMessage listneNotificationMessage();
    void sendNotificationProcessingResult(INotificationMessage message);
}
