package com.tsystems.notification.services;

import com.tsystems.models.notificationMessage.SecuredNotificationDto;

/**
 * The imple of this interface will recieive the secured notifications from queue and hold the transfomration or authentication if required
 */
public interface ISecuredNotificationService {
    SecuredNotificationDto getSecuredNotificationDto();

}
