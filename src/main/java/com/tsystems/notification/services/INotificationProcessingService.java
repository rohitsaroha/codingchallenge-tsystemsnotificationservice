package com.tsystems.notification.services;

import com.tsystems.models.notificationMessage.INotificationMessage;
import com.tsystems.models.notificationMessage.SecuredNotificationDto;

import java.util.concurrent.Callable;

/*
The interface Impl will keep track of failed requests and will dump it into a database.
it also hold the business logic for notificaiton service
 */
public interface INotificationProcessingService {
void processSecuredNotificationMessages(SecuredNotificationDto securedNotificationDto);
INotificationMessage processNotificationMessage();
void sendNotificationProcessingResult(INotificationMessage message);
}
