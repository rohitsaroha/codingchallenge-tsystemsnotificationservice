package com.tsystems.notification.services.impls;

import com.tsystems.models.notificationMessage.INotificationMessage;
import com.tsystems.models.notificationMessage.SecuredNotificationDto;
import com.tsystems.notification.infrastructure.persistence.repositories.NotificationProcessingResultRepository;
import com.tsystems.notification.services.INotificationProcessingService;
import com.tsystems.notification.services.IStreamNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The service holds the main business logic or transformation needed in notification service.
 * This service layer only interacts with the service layers of queues and streams not thier implementaiton directly
 */
@Service
public class NotificationProcessingServiceImpl implements INotificationProcessingService {
    private NotificationProcessingResultRepository notificationProcessingResultRepository;
    private IStreamNotificationService streamNotificationService;

    @Autowired
    public NotificationProcessingServiceImpl(NotificationProcessingResultRepository notificationProcessingResultRepository,StreamNotificationServiceImpl streamNotificationService){
    this.notificationProcessingResultRepository=notificationProcessingResultRepository;
    this.streamNotificationService=streamNotificationService;
    }

    @Override
    public INotificationMessage processNotificationMessage() {
        return null;
    }

    @Override
    public void sendNotificationProcessingResult(INotificationMessage message) {
        // send result to db
}

    @Override
    public void processSecuredNotificationMessages(SecuredNotificationDto securedNotificationDto) {
        streamNotificationService.sendNotificationProcessingResult(null);
    }
}

