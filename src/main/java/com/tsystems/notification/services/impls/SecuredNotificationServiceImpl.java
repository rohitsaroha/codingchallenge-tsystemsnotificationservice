package com.tsystems.notification.services.impls;

import com.tsystems.models.notificationMessage.SecuredNotificationDto;
import com.tsystems.notification.infrastructure.queues.IAuthenticatedNotificationMessageQueue;
import com.tsystems.notification.infrastructure.queues.INotificationsMessageQueue;
import com.tsystems.notification.services.ISecuredNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The sevice layer To recieive the secured notifications from queue and hold the transfomration or authentication if required
 */
@Service
public class SecuredNotificationServiceImpl implements ISecuredNotificationService {
    private IAuthenticatedNotificationMessageQueue authenticatedNotificationMessageQueue;
    private INotificationsMessageQueue notificationsMessageQueue;
@Autowired
public SecuredNotificationServiceImpl(IAuthenticatedNotificationMessageQueue authenticatedNotificationMessageQueue,INotificationsMessageQueue notificationsMessageQueue){
    this.authenticatedNotificationMessageQueue=authenticatedNotificationMessageQueue;
    this.notificationsMessageQueue=notificationsMessageQueue;
}
    @Override
    public SecuredNotificationDto getSecuredNotificationDto() {
        return authenticatedNotificationMessageQueue.getAuthenticatedNotificationMessage();
    }
}
