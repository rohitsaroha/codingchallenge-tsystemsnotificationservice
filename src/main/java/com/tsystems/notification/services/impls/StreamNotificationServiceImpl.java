package com.tsystems.notification.services.impls;

import com.tsystems.models.notificationMessage.INotificationMessage;
import com.tsystems.notification.infrastructure.persistence.repositories.NotificationProcessingResultRepository;
import com.tsystems.notification.infrastructure.queues.INotificationsMessageQueue;
import com.tsystems.notification.services.IStreamNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/*
The stream notificaiton service impl is the service impl of streams/chanels.
 It is required to do require transformetion or auth if number of channels increases
 */
@Service
public class StreamNotificationServiceImpl implements IStreamNotificationService {
    private INotificationsMessageQueue notificationsMessageQueue;
    private NotificationProcessingResultRepository notificationProcessingResultRepository;
    @Autowired
    public StreamNotificationServiceImpl(INotificationsMessageQueue notificationsMessageQueue,NotificationProcessingResultRepository notificationProcessingResultRepository){
        this.notificationProcessingResultRepository=notificationProcessingResultRepository;
        this.notificationsMessageQueue=notificationsMessageQueue;
    }

    @Override
    public INotificationMessage listneNotificationMessage() {
        return null;
    }

    @Override
    public void sendNotificationProcessingResult(INotificationMessage message) {

    }
}
