package com.tsystems.notification.exceptions;

public class NotificationDAOException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NotificationDAOException(String message) {
        super(message);
    }

    public NotificationDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
