package com.tsystems.notification.exceptions;

public class NotificationServiceException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public NotificationServiceException(String message) {
        super(message);
    }

    public NotificationServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
