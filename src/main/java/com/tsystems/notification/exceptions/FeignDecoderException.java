package com.tsystems.notification.exceptions;

public class FeignDecoderException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public FeignDecoderException(String message) {
    super(message);
  }

  public FeignDecoderException(String message, Throwable cause) {
    super(message, cause);
  }

}
