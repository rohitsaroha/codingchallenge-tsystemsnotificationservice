package com.tsystems.notification.config;

import com.tsystems.models.feign.FeignConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * The source of application configuration
 */
@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class AppConfig {
    private final FeignConfig clientDetails=new FeignConfig();

}
