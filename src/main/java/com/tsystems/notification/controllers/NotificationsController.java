package com.tsystems.notification.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The rest end points to access the services of notification micro service
 */
@RestController
@RequestMapping("/notifications")
public class NotificationsController {

}
