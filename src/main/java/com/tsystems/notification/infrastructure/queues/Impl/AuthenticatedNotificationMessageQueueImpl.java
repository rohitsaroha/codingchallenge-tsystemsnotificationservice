package com.tsystems.notification.infrastructure.queues.Impl;

import com.tsystems.models.notificationMessage.SecuredNotificationDto;
import com.tsystems.notification.infrastructure.queues.IAuthenticatedNotificationMessageQueue;
import com.tsystems.notification.services.INotificationProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
/*
The Queue impleentation which will have listners listning to notificaitons sent by lambda
 */
@Service
public class AuthenticatedNotificationMessageQueueImpl implements IAuthenticatedNotificationMessageQueue {
    private INotificationProcessingService notificationProcessingService;
    @Autowired
    public AuthenticatedNotificationMessageQueueImpl(INotificationProcessingService notificationProcessingService){
        this.notificationProcessingService=notificationProcessingService;
    }

    @Override
    public SecuredNotificationDto getAuthenticatedNotificationMessage() {
        // listner recieives messages and then send to main business service to transform/process it
        notificationProcessingService.processSecuredNotificationMessages(null);
        return null;
    }
    @PostConstruct
    private void intializeListners(){

    }
}
