package com.tsystems.notification.infrastructure.queues;

import com.tsystems.models.notificationMessage.SecuredNotificationDto;

/*
The Interface implementation will connect to the queues
 where lambda function will send authenticated notification messages.
 */
public interface IAuthenticatedNotificationMessageQueue {
    SecuredNotificationDto getAuthenticatedNotificationMessage();
   }
