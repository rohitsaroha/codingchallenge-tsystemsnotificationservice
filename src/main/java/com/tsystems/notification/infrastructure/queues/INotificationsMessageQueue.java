package com.tsystems.notification.infrastructure.queues;

import com.tsystems.models.notificationMessage.INotificationMessage;
/*
This Interface implementations will send and recievies the notifications from KAfka/Kinesis streams
 sent by Queues implementated by AuthenticatedNotification Impls

 */
public interface INotificationsMessageQueue {
    INotificationMessage getNotificationMessageFromNotificationChannels();
    void sendNotificationMessageToChannels(INotificationMessage notificationMessage);

}
