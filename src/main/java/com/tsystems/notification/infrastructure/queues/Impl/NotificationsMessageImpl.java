package com.tsystems.notification.infrastructure.queues.Impl;

import com.tsystems.models.notificationMessage.INotificationMessage;
import com.tsystems.notification.config.AppConfig;
import com.tsystems.notification.infrastructure.queues.INotificationsMessageQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
/*
The kafka/kinesis Streams  implementaiton to listen to the channels and send the authenticatedNotification message to appropriate channels in streams
 */
@Service
public class NotificationsMessageImpl implements INotificationsMessageQueue {
private AppConfig appConfig;
@Autowired
public NotificationsMessageImpl(AppConfig appConfig){
    this.appConfig=appConfig;
}
   @PostConstruct
   private void initializeChannelListenersAndProducers(){
    try {
        intializeStreamProducer();
        initlaizestreamConsumers();
    }
    catch(Exception e){
        // logging of exception
    }
   }
    private void intializeStreamProducer() {
    /**
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, PropertiesUtil.applicationProperties.getProperty(PropertiesConstants.IKafkaConstants.KAFKA_BROKERS));
        props.put(ProducerConfig.CLIENT_ID_CONFIG, PropertiesUtil.applicationProperties.getProperty(PropertiesConstants.IKafkaConstants.CLIENT_ID));
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
     **/
    }

    private void initlaizestreamConsumers(){

    }
    // Will be used to get messages notifications
    @Override
    public INotificationMessage getNotificationMessageFromNotificationChannels() {
        return null;
    }

    // wil beto send message to appropriate channels
    @Override
    public void sendNotificationMessageToChannels(INotificationMessage notificationMessage) {

    }
}
