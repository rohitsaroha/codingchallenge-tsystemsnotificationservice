package com.tsystems.notification.infrastructure.persistence.repositories;
/** The interface will be used to connect the redis cache,
 which will keep the track of limit of notification service
 For Example - in case of client ABC is subscribed to an email subscription plan of 100 emails in a day
 then we need a fast cache store to keep track of messages we ahve sent out.
 The subscription plan details wil also contains the validity days,
 so that we can set the time to expire which can be refreshed within a day
 or after some days depending on the subscription plan terms and conditions.
**/
public interface CacheRepository {
}
