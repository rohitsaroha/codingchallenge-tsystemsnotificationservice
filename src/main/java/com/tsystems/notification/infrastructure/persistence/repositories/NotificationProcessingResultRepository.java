package com.tsystems.notification.infrastructure.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

/*
The JPA repository will be used to dump success and failed notification messages, for reporting purposes.

 */
public interface NotificationProcessingResultRepository  {
}
