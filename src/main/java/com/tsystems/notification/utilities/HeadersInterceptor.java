package com.tsystems.notification.utilities;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class HeadersInterceptor implements RequestInterceptor {

  private static final String NOTIFICATION_SERVICE_USER_HEADER = "X-Notification-Service-User";

  @Override
  public void apply(RequestTemplate template) {
    template.header(NOTIFICATION_SERVICE_USER_HEADER, "Notification_Service");
  }

}
