package com.tsystems.notification.externalClients;

import com.tsystems.models.clientDetails.ClientDetailsDto;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.web.bind.annotation.*;
/*
The Feign Client that will be used to connect with client detail service to get the required details
 */
@Headers("Content-Type: application/json")
public interface ClientDetailsService {
    @RequestLine("GET /{clientId}")
    ClientDetailsDto getSubscribedClientDetails(@Param("clientId") String clientId);
   }
